<?php

use App\Desa;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DesaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Desa::truncate();
        Desa::create([
            'nama_desa'         => 'Klompang Barat',
            'kode_desa'         => '3528092002',
            'nama_kecamatan'    => 'Pakong',
            'kode_kecamatan'    => '352809',
            'nama_kabupaten'    => 'Pamekasan',
            'kode_kabupaten'    => '3528',
            'nama_provinsi'     => 'Jawa Timur',
            'kode_provinsi'     => '35',
            'kodepos'           => '69352',
            'alamat'            => 'Klompang Barat, Pakong',
            'nama_kepala_desa'  => "Samsul Arifin",
            'alamat_kepala_desa'=> "Klompang Barat, Pakong",
            'logo'              => "logo.png",
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
